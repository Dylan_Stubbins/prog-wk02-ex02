﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wk02_ex02
{
    class Program
    {
        static void Main(string[] args)
        {

            var menuSelection = Console.ReadLine();
            switch (menuSelection)
            {
                case "red":
                Console.WriteLine("You chose the color red");
                    break;

                case "blue":
                    Console.WriteLine("You chose the color blue");
                    break;

                default:
                    Console.WriteLine("You chose the wrong color");
                    break;
            }


        }
    }
}
